#!/usr/bin/env python
#
# happy birthday, mr. mentor. :)
#


import random
import time
import thread
import os
import sys


import turtle
import pygame.mixer

# Satify these guys first
pygame.mixer.init()
random.seed()

# turtle globals
TURTLE_SPEED = 2
TURTLE_SIZE = 15

# screen globals
SCREEN_WIDTH = 600
SCREEN_HEIGHT = 500

# sound file
SOUND_FILE = "data/music.wav"

def play_sound():
    path = os.path.abspath(SOUND_FILE)
    sound = pygame.mixer.Sound(path)
    channel = sound.play()
    while channel.get_busy():
        time.sleep(1)

class KustomTurtle(object):
    def __init__(self, params):
        self.t = turtle.Turtle()
        self.points = params
        self.t.ht()
        self.t.penup()
        self.t.speed(TURTLE_SPEED)
        self.t.pensize(TURTLE_SIZE)
        self.colors = ['red', 'purple', 'black']

    def spawn_random(self, screen_width, screen_height):
        x = screen_width * random.random()
        y = screen_height * random.random()

        # since the coordinate system of turtle places
        # its origin at the very center, generating 
        # random position get a bit trickey. 

        # my approach is to generate a random co-ordinate 
        # and then subtract half the screen size from it, to
        # effectively give me random co-ordinates in four 
        # coordinate quadrants
        x = x - (screen_width/2)
        y = y - (screen_height/2)

        # debug
        # print "spawning at {}, {}".format(x, y)

        self._spawn(x, y)

        # i'll use method chaining later
        return self

    def _get_random_color(self):
        i = random.randint(0, len(self.colors) -1)
        return self.colors[i]

    def _spawn(self, x, y):

        self.t.color(self._get_random_color())
        self.t.st()
        self._navigate2(x, y)

    def _navigate2(self, x, y):
        '''
        Use this method to move the turtle on the screen
        for all intents and purposes, this is your 'goto' method
        '''
        self.t.setheading(self.t.towards(x, y))
        self.t.goto(x, y)

    def go(self):
        p1 = self.points[0]
        p2 = self.points[1]

        self._navigate2(*p1)
        self.t.pendown()
        self._navigate2(*p2)
        self.t.penup()

        # move the turle below the screen.
        # bye little turtle-ly
        oldx, oldy = self.t.pos()
        #  the -300 is an added measure to make sure it goes off
        # screen
        self._navigate2(oldx, SCREEN_HEIGHT/2 * (-1) - 300)

if __name__ == "__main__":
    h = SCREEN_HEIGHT
    w = SCREEN_WIDTH

    # now if you're reading the source code, 
    # I'm going to pretend that I didn't spend
    # around 3 hours experimenting with interative
    # python sessions to find these coordinates out.
    # no way, why would any would do that, because
    # that would be stupid. pfft.
    strokes = [
        #borders
        [(w/2, h/2), (w/2, -h/2)],
        [(w/2, -h/2),(-w/2, -h/2)],
        [(-w/2, -h/2), (-w/2, h/2)],
        [(-w/2, h/2), (w/2, h/2)],
        # H
        [(-250, 200), (-250, 100)],
        [(-175, 200), (-175, 100)],
        [(-250, 150), (-175, 150)],
        # A
        [(-100, 200), (-135, 100)],
        [(-100, 200), (-65, 100)],
        [(-135, 150), (-65, 150)],
        # P
        [(-30, 200), (-30, 100)],
        [(-30, 200), (20, 165)],
        [(20, 165), (-30, 130)],
        # P
        [(50, 200), (50, 100)],
        [(50, 200), (100, 165)],
        [(100, 165), (50, 130)],
        # Y
        [(180, 200), (130, 100)],
        [(130, 200), (150, 150)],
        # B
        [(-250, 0), (-175, -50)],
        [(-250, 0), (-250, -200)],
        [(-175, -50), (-250, -100)],
        [(-250, -100), (-175, -150)],
        [(-175, -150), (-250, -200)],
        # apostrophe
        [(-130, 0), (-130, -40)],
        # D
        [(-70, -50), (-70, -150)],
        [(-70, -50), (-30, -83)],
        [(-30, -83), (-30, -113)],
        [(-30, -113), (-70, -150)],
        # A
        [(30, -50), (-5, -150)],
        [(30, -50), (65, -150)],
        [(-5, -100), (65, -100)],
        # Y
        [(145, -50), (115, -150)],
        [(105, -50), (125, -100)]

    ]

    # The hills are alive with the sound of music ...
    thread.start_new_thread(play_sound, ())
    t = [KustomTurtle(stroke).spawn_random(SCREEN_WIDTH, SCREEN_HEIGHT)
                                                   for stroke in strokes]

    random.shuffle(t)

    for k in t:
        k.go()

    time.sleep(5)

    t = turtle.Turtle()
    t.ht()

    t.screen.clear()

    t.write("No Turtles were harmed in the making of this program",
            align='center',
            font=('default', 17))
    t.goto(0, -30)
    t.write("KTHXBYE!", align='center')

    time.sleep(5)

    t.screen.clear()
    t.write("no srsly, go. Its over.",
            align='center',
            font=('default', 17))

    time.sleep(10)
